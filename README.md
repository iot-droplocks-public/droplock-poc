# Droplocks IoT project
## Steve Kerrison, James Cook University Singapore <steve.kerrison [at] jcu.edu.au>

This is a simple demo of the Droplocks IoT concept.

Initial code based on TTGO T8 display demo: https://github.com/Xinyuan-LilyGO/LilyGo-T-Display-S2

Implements Waveshare fingerprint reader basic functionality and relay control.

Ported to PlatformIO + ESP-IDF

This code uses the MIT License (see `LICENSE.txt`), except where otherwise stated, including the following considerations:

- The `LilyGo-T-Display-S2` has no license declared in its repository or sources
- The James Cook University logo is the property of James Cook University, Australia.