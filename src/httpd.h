#ifndef _HTTPD_H

#include <esp_http_server.h>

httpd_handle_t start_webserver(void);

#endif //_HTTPD_H